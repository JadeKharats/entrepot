require "spec"

# Helper methods for testing controllers (curl, with_server, context)
require "action-controller/spec_helper"

# Your application config
# If you have a testing environment, replace this with a test config file
require "../src/config"

def clean_database(name : String = "Pokemon")
  list_collections = MongoClient.database.list_collections(name_only: true).to_a
  list_collections.each do |collection|
    name = collection["name"].to_s
    MongoClient.database.command(Mongo::Commands::Drop, name: name)
  end
rescue
  "already clean"
end

def init_one_dataset(name : String = "Pokemon", raw_data : String = "Pikachu")
  collection = MongoClient.database[name]
  ds_namedtuple = {
    uuid:        "efeffefe",
    format:      "text",
    environment: "staging",
    raw_data:    raw_data,
  }
  collection.insert_one(ds_namedtuple)
end
