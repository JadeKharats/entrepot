require "./spec_helper"

describe Welcome do
  # ==============
  #  Unit Testing
  # ==============
  it "should generate a date string" do
    # instantiate the controller you wish to unit test
    welcome = Welcome.spec_instance(HTTP::Request.new("GET", "/"))

    # Test the instance methods of the controller
    welcome.set_date_header.should contain("GMT")
  end

  # ==============
  # Test Responses
  # ==============
  client = AC::SpecHelper.client

  # optional, use to change the response type
  # headers = HTTP::Headers{
  #   "Accept" => "application/json",
  # }

  it "should welcome you with json" do
    result = client.get("/")
    json_body = JSON.parse result.body
    json_body["message"].should eq %(Welcome to the Entrepot)
    json_body["source_url"].should eq %(https://gitlab.com/JadeKharats/entrepot)
    json_body["openapi_url"].should eq %(/openapi/swagger)
    json_body["contact"].should eq %(Jade D. Kharats)
    result.headers["Date"].should_not be_nil
  end
end
