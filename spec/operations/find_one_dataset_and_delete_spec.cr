require "../spec_helper"

describe FindOneDatasetAndDelete do
  describe "#run" do
    it "raise an exception if no dataset" do
      clean_database
      expect_raises(Exception, "No content") do
        FindOneDatasetAndDelete.new.run("Pokemon")
      end
    end
    it "return a pokemon dataset" do
      init_one_dataset
      dataset = FindOneDatasetAndDelete.new.run("Pokemon")
      dataset.name.should eq "Pokemon"
      dataset.uuid.should eq "efeffefe"
      dataset.format.should eq "text"
      dataset.environment.should eq "staging"
      dataset.raw_data.should eq "Pikachu"
    end
  end
end
