require "../spec_helper"

describe Purge do
  describe "#run" do
    it "remove collection with name" do
      clean_database
      3.times { init_one_dataset(name: "Talk Show") }
      5.times { init_one_dataset(name: "Movies") }
      7.times { init_one_dataset(name: "Tv Show") }
      result = Purge.new.run("Tv Show")
      result.should be_a Mongo::Commands::Common::BaseResult
    end

    it "raise exception when no collection with name" do
      clean_database
      expect_raises(Exception, "Collection doesn't exist") do
        Purge.new.run("Pokemon")
      end
    end
  end
end
