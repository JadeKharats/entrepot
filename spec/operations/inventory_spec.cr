require "../spec_helper"

describe Inventory do
  describe "#run" do
    it "return the list of collection with count" do
      clean_database
      3.times { init_one_dataset(name: "Talk Show") }
      5.times { init_one_dataset(name: "Movies") }
      7.times { init_one_dataset(name: "Tv Show") }
      result = Inventory.new.run
      result.should be_a DatasetList
      movie = result.datasets[0]
      talk_show = result.datasets[1]
      tv_show = result.datasets[2]
      movie.name.should eq "Movies"
      talk_show.name.should eq "Talk Show"
      tv_show.name.should eq "Tv Show"
      movie.quantity.should eq 5
      talk_show.quantity.should eq 3
      tv_show.quantity.should eq 7
    end
  end
end
