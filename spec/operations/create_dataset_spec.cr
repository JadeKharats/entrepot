require "../spec_helper"

describe CreateDataset do
  describe "#run" do
    dataset = Dataset.new("Pokemon", "staging", "text", "Pikachu")
    it "store dataset in collection Pokemon" do
      result = CreateDataset.new.run(dataset)
      result.should be_a Mongo::Commands::Common::InsertResult
    end
  end
end
