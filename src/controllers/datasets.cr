class Datasets < Application
  base "/api/datasets"

  # Add a dataset to Entrepot.
  # A dataset have a name, an environment, a format and his raw_data
  @[AC::Route::POST("/", body: :dataset_input)]
  def create(dataset_input : DatasetInput) : DatasetOutput
    dataset = dataset_input.to_dataset
    CreateDataset.new.run(dataset)
    DatasetOutput.new(dataset.uuid, dataset.name, dataset.environment, dataset.format, dataset.raw_data)
  end

  # List of datasets availables
  @[AC::Route::GET("/")]
  def list : DatasetList
    Inventory.new.run
  end

  # Get a dataset with the name of his category
  @[AC::Route::GET("/:name")]
  def buy(name : String) : DatasetOutput
    dataset = FindOneDatasetAndDelete.new.run(name)
    DatasetOutput.new(dataset.uuid, dataset.name, dataset.environment, dataset.format, dataset.raw_data)
  end

  # Remove collection of dataset by his name
  @[AC::Route::DELETE("/:name")]
  def remove_all(name : String) : DatasetList
    Purge.new.run(name)
    Inventory.new.run
  end
end
