# description of the welcome klass
class Welcome < Application
  base "/"

  class Message
    include JSON::Serializable

    property message : String
    property source_url : String
    property openapi_url : String
    property contact : String

    def initialize(
      @message = "Welcome to the Entrepot",
      @source_url = "https://gitlab.com/JadeKharats/entrepot",
      @openapi_url = "/openapi/swagger",
      @contact = App::CONTACT
    )
    end
  end

  # A welcome message
  @[AC::Route::GET("/")]
  def index : Message
    Log.warn { "logs can be collated using the request ID" }

    # You can use signals to change log levels at runtime
    # USR1 is debugging, USR2 is info
    # `kill -s USR1 %APP_PID`
    Log.debug { "use signals to change log levels at runtime" }

    render json: Message.new
  end
end
