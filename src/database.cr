require "cryomongo"

# module MongoClient
#   extend self

#   def self.database
#     client = Mongo::Client.new(App::DATABASE_URL)
#     client[App::DATABASE_NAME]
#   end

# end

class MongoClient
  property database : Mongo::Database

  def initialize
    client = Mongo::Client.new(App::DATABASE_URL)
    @database = client[App::DATABASE_NAME]
  end

  def self.database
    self.instance.database
  end

  def self.instance
    @@instance ||= new
  end
end
