class DatasetInput
  include JSON::Serializable

  property name : String
  property environment : String
  property format : String
  property raw_data : String

  def to_dataset
    Dataset.new(@name, @environment, @format, @raw_data)
  end
end
