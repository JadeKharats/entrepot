class DatasetOutput
  include JSON::Serializable

  property uuid : String
  property name : String
  property environment : String
  property format : String
  property raw_data : String

  def initialize(@uuid, @name, @environment, @format, @raw_data)
  end
end
