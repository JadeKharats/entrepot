class DatasetDetailsStore
  include JSON::Serializable

  property name : String
  property quantity : Int64

  def initialize(@name, @quantity)
  end
end
