class DatasetList
  include JSON::Serializable

  property datasets : Array(DatasetDetailsStore)

  def initialize
    @datasets = Array(DatasetDetailsStore).new
  end
end
