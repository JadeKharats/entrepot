require "uuid"

class Dataset
  property uuid : String
  property name : String
  property environment : String
  property format : String
  property raw_data : String

  def initialize(@name, @environment, @format, @raw_data, @uuid = UUID.random.to_s)
  end
end
