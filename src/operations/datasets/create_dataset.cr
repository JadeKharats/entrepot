class CreateDataset
  def run(dataset : Dataset)
    collection = MongoClient.database[dataset.name]
    ds_namedtuple = {
      uuid:        dataset.uuid,
      format:      dataset.format,
      environment: dataset.environment,
      raw_data:    dataset.raw_data,
    }
    collection.insert_one(ds_namedtuple)
  end
end
