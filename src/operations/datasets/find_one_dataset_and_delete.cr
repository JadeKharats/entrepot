class FindOneDatasetAndDelete
  def run(name : String)
    collection = MongoClient.database[name]
    document = collection.find_one_and_delete(NamedTuple.new)
    if document
      Dataset.new(
        name,
        document["environment"].to_s,
        document["format"].to_s,
        document["raw_data"].to_s,
        document["uuid"].to_s
      )
    else
      raise Exception.new("No content")
    end
  end
end
