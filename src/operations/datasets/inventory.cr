class Inventory
  def run
    list_collections = MongoClient.database.list_collections(name_only: true).to_a
    datasetlist = DatasetList.new
    list_collections.each do |collection|
      name = collection["name"].to_s
      datasetlist.datasets << DatasetDetailsStore.new(
        name,
        MongoClient.database[name].count_documents
      )
    end
    datasetlist.datasets.sort_by!(&.name)
    datasetlist
  end
end
