class Purge
  def run(name : String)
    MongoClient.database.command(Mongo::Commands::Drop, name: name)
  rescue
    raise Exception.new("Collection doesn't exist")
  end
end
